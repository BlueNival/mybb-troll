document.addEventListener('DOMContentLoaded', function() {

    chrome.storage.local.get('mybb_troll', function (result) {
        var html = '';
        if (result && Array.isArray(result.mybb_troll)) {
            Array.prototype.forEach.call(result.mybb_troll, function (el) {
                html += '<div class="trollFieldset"><label>Форум <input type="text" class="forum" value="' + el.forum + '"/></label><label>Пользователи <input type="text" class="users" value="' + el.users + '" /></label></div>';
            })
        }
        html +='<div class="trollFieldset"><label>Форум <input type="text" class="forum" /></label><label>Пользователи <input type="text" class="users" /></label></div>'
        document.getElementById('fields').innerHTML = html;
    });

    var checkPageButton = document.getElementById('trollsSave');
    checkPageButton.addEventListener('click', function() {
        var trolls = [];
        var els = document.getElementsByClassName("trollFieldset");
        Array.prototype.forEach.call(els, function (el) {
            var forum = el.getElementsByClassName('forum')[0].value;
            var users = el.getElementsByClassName('users')[0].value;
            if (forum) {
                trolls.push({forum: forum, users: users})
            }
        });
        chrome.storage.local.set({'mybb_troll': trolls});
        chrome.storage.local.get('mybb_troll', function (result) {
        });
    })
}, false);

