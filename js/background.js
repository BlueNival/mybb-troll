var fullUrl = window.location.href;
if (fullUrl.indexOf('viewtopic.php') !== -1) {
    chrome.storage.local.get('mybb_troll', function (result) {
        var currentUrl = window.location.origin;
        Array.prototype.forEach.call(result.mybb_troll, function (el) {
            if (currentUrl.localeCompare(el.forum) === 0) {
                var names = el.users.split(',');
                names = names.map(function (name) {
                    return name.trim().toLocaleLowerCase();
                })
                var authors = document.querySelectorAll('.pa-author');
                Array.prototype.forEach.call(authors, function (author) {
                    if (author.innerText) {
                        if (names.indexOf(author.innerText.toLocaleLowerCase()) !== -1) {
                            var post = author.parentNode.parentNode.parentNode.parentNode;
                            var id = post.getAttribute('id');
                            document.getElementById(id).remove();
                        }
                    }
                })
            }
        })
    });
}
